// std and main are not available for bare metal software
#![no_std]
#![no_main]

use cortex_m_rt::entry;
use stm32f1::stm32f100;

#[entry]
fn main() -> ! {
    // get handles to the hardware
    let peripherals = stm32f100::Peripherals::take().unwrap();
    let gpioc = &peripherals.GPIOC;
    let rcc = &peripherals.RCC;

    // enable the GPIO clock for IO port C
    rcc.apb2enr.write(|w| w.iopcen().set_bit());

    gpioc.crh.write(|w| {
        w.mode8().bits(0b11);
        w.cnf8().bits(0b00);
        w.mode9().bits(0b11);
        w.cnf9().bits(0b00)
    });

    loop {
        gpioc.bsrr.write(|w| w.bs8().set_bit());
        gpioc.brr.write(|w| w.br9().set_bit());
        cortex_m::asm::delay(1000000);

        gpioc.bsrr.write(|w| w.bs9().set_bit());
        gpioc.brr.write(|w| w.br8().set_bit());
        cortex_m::asm::delay(1000000);
    }
}

#[panic_handler]
fn my_panic(_info: &core::panic::PanicInfo) -> ! { loop {} }
